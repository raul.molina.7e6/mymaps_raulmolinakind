package com.example.mymaps

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.impl.ImageOutputConfig.RotationValue
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.mymaps.databinding.Fragment1Binding
import com.example.mymaps.databinding.FragmentDetalleBinding
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class DetalleFragment : Fragment() {
    private val db = FirebaseFirestore.getInstance()
    private val args: DetalleFragmentArgs by navArgs()
    lateinit var binding: FragmentDetalleBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetalleBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.nombre.text = "Nombre: ${args.nombre}"
        binding.latitud.text = "Lat: ${args.latitud}"
        binding.longitud.text = "Lon: ${args.longitud}"
        binding.uso.text = "Uso: ${args.usoMarcador}"

        val storage = FirebaseStorage.getInstance().reference.child("images/${args.nombre}")
        val localFile = File.createTempFile("temp", "jpeg")
        storage.getFile(localFile).addOnSuccessListener {
            val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
            binding.foto.setImageBitmap(bitmap)
            binding.foto.rotation = 90f

        }.addOnFailureListener{
            Toast.makeText(requireContext(), "Error downloading image!", Toast.LENGTH_SHORT).show()
        }

        binding.botonBorrar.setOnClickListener {
            db.collection("/marcadores").document(args.nombre).delete()
            FirebaseStorage.getInstance().reference.child("images/${args.nombre}").delete()
            val action = DetalleFragmentDirections.actionDetalleFragmentToFragment1()
            findNavController().navigate(action)
        }
    }
}
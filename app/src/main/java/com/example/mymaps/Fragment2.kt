package com.example.mymaps

import android.R
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.mymaps.databinding.Fragment2Binding
import com.google.android.material.navigation.NavigationView
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.text.SimpleDateFormat
import java.util.*


class Fragment2 : Fragment() {
    private val db = FirebaseFirestore.getInstance()
    lateinit var binding: Fragment2Binding
    private val args: Fragment2Args by navArgs()
    lateinit var uriFoto: Uri

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = Fragment2Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(args.urlFoto != ""){
            binding.foto.setImageURI(args.urlFoto.toUri())
        }

        binding.camara.setOnClickListener {
            val action = Fragment2Directions.actionFragment2ToCamaraFragment(args.lat, args.lon)
            findNavController().navigate(action)
        }

        binding.galeria.setOnClickListener {
            openGalleryForImages()
        }

        binding.botonCancelar.setOnClickListener {
            val action = Fragment2Directions.actionFragment2ToMapFragment()
            findNavController().navigate(action)
        }

        binding.botonMarcar.setOnClickListener {
            if(binding.nombre.text.toString() == ""){
                Toast.makeText(requireContext(), "El nombre no puede estar vacio", Toast.LENGTH_SHORT).show()
            }else{
                val navigationView = requireActivity().findViewById(com.example.mymaps.R.id.navigationView) as NavigationView
                val headerView = navigationView.getHeaderView(0)
                val navUsername = headerView.findViewById<View>(com.example.mymaps.R.id.nombreUsuario) as TextView
                val usuario = navUsername.text.toString()
                db.collection("/marcadores").document(binding.nombre.text.toString()).set(
                    hashMapOf("latitud" to args.lat.toString(),
                        "longitud" to args.lon.toString(),
                        "nombre" to binding.nombre.text.toString(),
                        "rutaFoto" to "",
                        "usoMarcador" to binding.spinner.selectedItem.toString(),
                        "usuario" to usuario)
                )

                val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
                val now = Date()
                val fileName = formatter.format(now)
                val storage = FirebaseStorage.getInstance().getReference("images/${binding.nombre.text}")
                if(args.urlFoto != "") uriFoto = args.urlFoto.toUri()
                storage.putFile(uriFoto)
                    .addOnSuccessListener {
                        binding.foto.setImageURI(null)
                        Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener {
                        Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
                    }

                Toast.makeText(requireContext(), "Marcador añadido", Toast.LENGTH_SHORT).show()
                val action = Fragment2Directions.actionFragment2ToMapFragment()
                findNavController().navigate(action)
            }
        }
    }
    fun openGalleryForImages() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"
        resultLauncher.launch(intent)
    }
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if(result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            val imageList = listOf<ImageView>(binding.foto)

            if(data?.getData() != null){
                var imageUri: Uri = data.data!!
                uriFoto = data.data!!
                imageList[0].setImageURI(imageUri)
            }

        }
    }
}


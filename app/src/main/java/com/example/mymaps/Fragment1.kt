package com.example.mymaps

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mymaps.databinding.Fragment1Binding
import com.google.android.material.navigation.NavigationView
import com.google.firebase.firestore.*

class Fragment1 : Fragment(), MyOnClickListener {
    private val db = FirebaseFirestore.getInstance()
    private lateinit var marcadorAdapter: MarcadorAdapter
    lateinit var marcadorList: ArrayList<Marcador>
    lateinit var binding: Fragment1Binding
    lateinit var usuario: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = Fragment1Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView(arrayListOf())
        marcadorList = arrayListOf<Marcador>()
        eventChangeListener()

        val navigationView = requireActivity().findViewById(com.example.mymaps.R.id.navigationView) as NavigationView
        val headerView = navigationView.getHeaderView(0)
        val navUsername = headerView.findViewById<View>(com.example.mymaps.R.id.nombreUsuario) as TextView
        usuario = navUsername.text.toString()
    }


    private fun setupRecyclerView(usersList: List<Marcador>) {

        marcadorAdapter = MarcadorAdapter(usersList, this)
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = marcadorAdapter
        }
    }

    private fun eventChangeListener() {
        db.collection("/marcadores").addSnapshotListener(object: EventListener<QuerySnapshot> {
            override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                if(error != null){
                    Log.e("Firestore error", error.message.toString())
                    return
                }
                for(dc: DocumentChange in value?.documentChanges!!){
                    if(dc.type == DocumentChange.Type.ADDED){
                        val newMarcador = Marcador(dc.document.get("latitud") as String,
                            dc.document.get("longitud") as String,
                            dc.document.get("nombre") as String,
                            dc.document.get("rutaFoto") as String,
                            dc.document.get("usoMarcador") as String,
                            dc.document.get("usuario") as String
                        )

                        newMarcador.nombre = dc.document.id

                        if(newMarcador.usuario == usuario){
                            marcadorList.add(newMarcador)
                        }

                    }
                }
                marcadorAdapter.setMarcadorList(marcadorList)
            }
        })
    }

    override fun onClick(marcador: Marcador) {
        val action = Fragment1Directions.actionFragment1ToDetalleFragment(marcador.latitud, marcador.longitud, marcador.nombre, marcador.rutaFoto, marcador.usoMarcador)
        findNavController().navigate(action)
    }
}
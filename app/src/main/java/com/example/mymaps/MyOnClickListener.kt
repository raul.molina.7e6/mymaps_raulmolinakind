package com.example.mymaps

import android.view.View.OnClickListener

interface MyOnClickListener {
    fun onClick(marcador: Marcador)
}
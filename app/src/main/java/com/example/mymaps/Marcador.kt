package com.example.mymaps

data class Marcador(
    var latitud: String,
    var longitud: String,
    var nombre: String,
    var rutaFoto: String,
    var usoMarcador: String,
    val usuario: String
)

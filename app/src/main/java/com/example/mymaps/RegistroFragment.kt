package com.example.mymaps

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.provider.Settings.Global.putString
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.edit
import androidx.navigation.fragment.findNavController
import com.example.mymaps.databinding.Fragment2Binding
import com.example.mymaps.databinding.FragmentRegistroBinding
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RegistroFragment : Fragment() {
    lateinit var binding: FragmentRegistroBinding
    lateinit var myPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegistroBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        myPreferences = requireActivity().getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)

        var auth = FirebaseAuth.getInstance()


        setupForm()
        //REGISTRTO
        binding.botonRegistro.setOnClickListener{
            FirebaseAuth.getInstance().
            createUserWithEmailAndPassword(binding.correoElectronico.text.toString(), binding.contrasenya.text.toString())
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        val emailLogged = it.result?.user?.email
                        goToHome(emailLogged!!)
                        rememberUser(binding.correoElectronico.text.toString(), binding.contrasenya.text.toString(), binding.rememberMe.isChecked)
                        val action = RegistroFragmentDirections.actionRegistroFragmentToMapFragment()
                        findNavController().navigate(action)
                    }
                    else{
                        Toast.makeText(requireContext(), "Email o contraseña no validos", Toast.LENGTH_SHORT).show()
                    }
                    auth.currentUser?.email
                }
        }
        //LOGIN
        binding.botonLogin.setOnClickListener {
            FirebaseAuth.getInstance().
            signInWithEmailAndPassword(binding.correoElectronico.text.toString(), binding.contrasenya.text.toString())
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        val emailLogged = it.result?.user?.email
                        goToHome(emailLogged!!)
                        rememberUser(binding.correoElectronico.text.toString(), binding.contrasenya.text.toString(), binding.rememberMe.isChecked)
                        val action = RegistroFragmentDirections.actionRegistroFragmentToMapFragment()
                        findNavController().navigate(action)
                    }
                    else{
                        Toast.makeText(requireContext(), "Email o contraseña no validos", Toast.LENGTH_SHORT).show()
                    }

                }
        }


    }
    fun goToHome(email: String){
        val navigationView = requireActivity().findViewById(com.example.mymaps.R.id.navigationView) as NavigationView
        val headerView = navigationView.getHeaderView(0)
        val navUsername = headerView.findViewById<View>(R.id.nombreUsuario) as TextView
        navUsername.text = email.split("@")[0]
    }

    private fun rememberUser(email: String, pass: String, remember: Boolean) {
        if(remember){
            myPreferences.edit {
                putString("email", email)
                putString("password", pass)
                putBoolean("remember", remember)
                putBoolean("active", remember)
                apply()
            }
        }else{
            myPreferences.edit {
                putString("email", "")
                putString("password", "")
                putBoolean("remember", remember)
                putBoolean("active", remember)
                apply()
            }
        }

    }
    private fun setupForm() {
        val email = myPreferences.getString("email", "")
        val pass = myPreferences.getString("password", "")
        val remember = myPreferences.getBoolean("remember", false)
        if (email != null) {
            if(email.isNotEmpty()){
                binding.correoElectronico.setText(email)
                binding.contrasenya.setText(pass)
                binding.rememberMe.isChecked = remember
            }
        }
    }

}

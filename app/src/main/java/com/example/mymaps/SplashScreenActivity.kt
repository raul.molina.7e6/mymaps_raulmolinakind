package com.example.mymaps

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.example.mymaps.databinding.ActivityMainBinding
import com.example.mymaps.databinding.ActivitySplashScreenBinding

class SplashScreenActivity : AppCompatActivity() {
    lateinit var binding: ActivitySplashScreenBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val splashScreen = installSplashScreen()
        setContentView(R.layout.activity_splash_screen)
        splashScreen.setKeepOnScreenCondition{ true }
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
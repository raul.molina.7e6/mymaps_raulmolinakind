package com.example.mymaps

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mymaps.databinding.ItemUserBinding

class MarcadorAdapter(private var marcadores: List<Marcador>, private val listener: MyOnClickListener):
    RecyclerView.Adapter<MarcadorAdapter.ViewHolder>() {

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemUserBinding.bind(view)
        fun setListener(user: Marcador){
            binding.root.setOnClickListener {
                listener.onClick(user)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return marcadores.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val marcador = marcadores[position]
        with(holder){
            setListener(marcador)
            binding.nombreMarcador.text = marcador.nombre
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setMarcadorList(lista: List<Marcador>){
        marcadores = lista
        notifyDataSetChanged()
    }
}